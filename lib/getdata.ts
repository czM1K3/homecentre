import { TempHumData } from "./types";

export const getTempHumData = async (ip: string) =>
    (await fetch(ip)
        .then((x) => x.json())
        .catch(() => null)) as TempHumData;
