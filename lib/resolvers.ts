import { AuthenticationError, UserInputError } from "apollo-server-micro";
import { PubSub } from "graphql-subscriptions";
import { createUser, findUser, validatePassword, getAllUsers } from "./user";
import { setLoginSession, getLoginSession } from "./auth";
import { removeTokenCookie } from "./auth-cookies";
import { connectToDatabase } from "../utils/mongodb";
import { getTempHumData } from "./getdata";

const pubsub = new PubSub();

const subAll = async () => {
    const { db } = await connectToDatabase();
    const response = await db
        .collection("widgets")
        .find({ type: "TempHum" })
        .toArray();
    response.map((x) => {
        setInterval(
            async () => pubsub.publish(x.name, await getTempHumData(x.ip)),
            1000
        );

        setInterval(async () => {
            const tempHumData = await getTempHumData(x.ip);
            await db.collection("widgets").updateOne(
                { ip: x.ip },
                {
                    $push: {
                        history: {
                            $each: [
                                {
                                    temp: tempHumData.temp,
                                    humidity: tempHumData.humidity,
                                    date: Math.floor(
                                        new Date().getTime() / 1000
                                    ),
                                },
                            ],
                            $sort: { date: -1 },
                            $slice: 10,
                        },
                    },
                }
            );
        }, 10000);
    });
};
subAll();

const resolvers = {
    Query: {
        hello: () => {
            return "Hello!";
        },
        tempHum: async () => await getTempHumData("http://192.168.1.27/"),
        allTempHum: async () => {
            const { db } = await connectToDatabase();
            const response = await db
                .collection("widgets")
                .find({ type: "TempHum" })
                .toArray();
            return response;
        },
        viewer: async (_parent, _args, context) => {
            try {
                const session = await getLoginSession(context.req);

                if (session) {
                    return findUser({ username: session.username });
                }
            } catch (error) {
                throw new AuthenticationError(
                    "Authentication token is invalid, please log in"
                );
            }
        },
        users: async () => getAllUsers(),
    },
    Mutation: {
        signUp: async (_parent, args, _context, _info) => {
            const user = await createUser(args.input);
            return { user };
        },
        signIn: async (_parent, args, context, _info) => {
            const user = await findUser({ username: args.input.username });
            if (user && (await validatePassword(user, args.input.password))) {
                const session = {
                    id: user.id,
                    username: user.username,
                };
                await setLoginSession(context.res, session);
                return { user };
            }
            throw new UserInputError(
                "Invalid username and password combination"
            );
        },
        signOut: async (_parent, _args, context, _info) => {
            removeTokenCookie(context.res);
            return true;
        },
        addTempHum: async (_parent, args, _context, _info) => {
            const { db } = await connectToDatabase();
            const response = await db.collection("widgets").insertOne({
                type: "TempHum",
                name: args.input.name,
                ip: args.input.ip,
                history: [],
            });
            return response.result.ok;
        },
    },
    Subscription: {
        tempHum: {
            subscribe: async (_, { name }) => await pubsub.asyncIterator(name),
            resolve: (data) => data,
        },
    },
};
export default resolvers;
