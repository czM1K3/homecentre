export type TempHumData = {
    temp: number;
    humidity: number;
};
