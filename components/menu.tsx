import { FC } from "react";
import Link from "next/link";
import { FaPlus } from "react-icons/fa";

const header = "Home centre";

type menuProps = {
    UserName: string;
};

const Menu: FC<menuProps> = ({ UserName }) => {
    return (
        <>
            <div className="h-12 w-screen bg-gray-900">
                <Link href="/adddevice">
                    <a>
                        <FaPlus className="absolute h-12 ml-3" />
                    </a>
                </Link>
                <h1 className="font-sans text-center m-0 capitalize text-4xl">
                    {header}
                </h1>
                <div className="bg-blue-900 w-max h-12 absolute top-0 right-0">
                    <Link href="/logout">
                        <div className="bg-blue-600 p-3">
                            <p>{UserName}</p>
                        </div>
                    </Link>
                </div>
            </div>
        </>
    );
};
export default Menu;
