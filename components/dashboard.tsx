// import PowerSocket from "./widgets/powersocket";
// import Light from "./widgets/light";
import { FC } from "react";
import TempHum from "./widgets/temphum/temphum";
import { useWidgetsQueryQuery } from "../graphql/widgets.graphql";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";

const useStyles = makeStyles((theme) => ({
    flex: {
        justifyContent: "center",
        alignItems: "center",
        flexWrap: "wrap",
        display: "flex",
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
}));

const Dashboard: FC = () => {
    const classes = useStyles();

    const { data, loading, error } = useWidgetsQueryQuery();

    if (loading) return <h1>Loading...</h1>;
    if (error) {
        return <h1>Error...</h1>;
    }

    return (
        <Container maxWidth="lg" className={classes.container}>
            <Grid container spacing={3}>
                {/* <PowerSocket name="3D tiskárna" />
                <PowerSocket name="Lednička" />
                <Light name="Pokoj 1" /> */}
                {data.allTempHum.map(({ name, ip, history }) => {
                    return (
                        <TempHum
                            name={name}
                            ip={ip}
                            key={ip}
                            history={history}
                        />
                    );
                })}
            </Grid>
        </Container>
    );
};
export default Dashboard;
