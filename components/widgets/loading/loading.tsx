import { FC } from "react";
import ReactLoading from "react-loading";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    main: {
        minHeight: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
}));

const Loading: FC = () => {
    const classes = useStyles();
    return (
        <div className={classes.main}>
            <ReactLoading
                type="bars"
                color="#3F51B5"
                height={"30%"}
                width={"30%"}
            />
        </div>
    );
};

export default Loading;
