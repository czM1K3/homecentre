import { FC } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        display: "flex",
        overflow: "auto",
        flexDirection: "column",
    },
}));

const Base: FC = ({ children }) => {
    const classes = useStyles();

    return (
        <Grid item xs={2} md={2} lg={2}>
            <Paper className={classes.paper}>{children}</Paper>
        </Grid>
    );
};
export default Base;
