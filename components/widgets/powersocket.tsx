import { FC } from "react";
import Base from "./base";
import { FaPowerOff } from "react-icons/fa";

type powerSockerProps = {
    name: string;
};

const PowerSocket: FC<powerSockerProps> = ({ name }) => {
    return (
        <Base>
            <h2 className="text-center">{name}</h2>
            <FaPowerOff
                size="80"
                className="text-blue-200 mx-auto my-5 h-24 block"
            />
        </Base>
    );
};
export default PowerSocket;
