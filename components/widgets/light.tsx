import { FC } from "react";
import Base from "./base";
import { FaLightbulb } from "react-icons/fa";

type lightProps = {
    name: string;
};

const Light: FC<lightProps> = ({ name }) => {
    return (
        <Base>
            <h1 className="text-center">{name}</h1>
            <FaLightbulb
                size="80"
                className="text-blue-200 mx-auto my-5 h-24 block"
            />
        </Base>
    );
};
export default Light;
