import { FC, useState } from "react";
import Base from "../base";
import { FaThermometerHalf } from "react-icons/fa";
import { ImDroplet } from "react-icons/im";
import { useTempHumSubSubscription } from "../../../graphql/tempHum.graphql";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Slide from "@material-ui/core/Slide";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import { MdClose } from "react-icons/md";
import { TempHumHistory } from "../../../graphql/widgets.graphql";
import Chart from "./charts";

type tempHumProps = {
    name: string;
    ip: string;
    history: TempHumHistory[];
};

const useStyles = makeStyles(() => ({
    item: {
        margin: "1.25rem 0",
        fontSize: "1.125rem",
        lineHeight: "1.75rem",
    },
    icon: {
        color: "rgba(191, 219, 254)",
        height: "2.5rem",
        display: "inline",
    },
    center: {},
    modal: {},
    modalHeader: {},
}));

const TempHum: FC<tempHumProps> = ({ name, ip, history }) => {
    const classes = useStyles();
    const [modal, setModal] = useState(false);

    const dialogClasses = {
        root: classes.center,
        paper: classes.modal,
    };

    const { data, loading, error } = useTempHumSubSubscription({
        variables: { name },
    });
    if (loading)
        return (
            <Base>
                <h1>Loading...</h1>
            </Base>
        );
    if (error) {
        return (
            <Base>
                <h1>Error...</h1>
            </Base>
        );
    }
    if (!data.tempHum)
        return (
            <Base>
                <h1>No data...</h1>
            </Base>
        );

    return (
        <Base>
            <Typography
                component="h2"
                variant="h6"
                color="primary"
                gutterBottom
                onClick={() => setModal(true)}
            >
                {name}
            </Typography>
            <p className={classes.item}>
                <FaThermometerHalf size="30" className={classes.icon} /> :{" "}
                {data.tempHum.temp}°
            </p>
            <p className={classes.item}>
                <ImDroplet size="30" className={classes.icon} /> :{" "}
                {data.tempHum.humidity}%
            </p>
            <Dialog
                classes={dialogClasses}
                open={modal}
                keepMounted
                onClose={() => setModal(false)}
                aria-labelledby="modal-slide-title"
                aria-describedby="modal-slide-description"
            >
                <DialogTitle
                    id="classic-modal-slide-title"
                    disableTypography
                    className={classes.modalHeader}
                >
                    <Button
                        variant="contained"
                        color="secondary"
                        onClick={() => setModal(false)}
                    >
                        <MdClose />
                    </Button>
                </DialogTitle>
                <DialogContent id="modal-slide-description">
                    {/* <p>{history.map(x => new Date(x.date * 1000) + " ")}</p> */}
                    <Chart history={history} />
                </DialogContent>
                {/* <DialogActions
                    className={
                        classes.modalFooter + " " + classes.modalFooterCenter
                    }
                >
                    <Button onClick={() => setModal(false)}>Never Mind</Button>
                </DialogActions> */}
            </Dialog>
        </Base>
    );
};
export default TempHum;
