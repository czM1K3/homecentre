import { FC, useMemo } from "react";
import { Chart } from "react-charts";
import { TempHumHistory } from "../../../graphql/widgets.graphql";

type MyChartProps = {
    history: TempHumHistory[];
};

const MyChart: FC<MyChartProps> = ({ history }) => {
    const data = useMemo(
        () => [
            {
                label: "Temperature",
                data: history.map(({ temp, date }) => [
                    new Date(date * 1000).getTime(),
                    temp,
                ]),
            },
            {
                label: "Humidity",
                data: history.map(({ humidity, date }) => [
                    new Date(date * 1000).getTime(),
                    humidity,
                ]),
            },
        ],
        []
    );
    const axes = useMemo(
        () => [
            { primary: true, type: "time", position: "bottom" },
            { type: "linear", position: "left" },
        ],
        []
    );
    return (
        <div
            style={{
                width: "550px",
                height: "400px",
            }}
        >
            <Chart data={data} axes={axes} />
        </div>
    );
};

export default MyChart;
