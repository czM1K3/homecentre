import { FC } from "react";
import { useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { useApolloClient } from "@apollo/client";
import { getErrorMessage } from "../lib/form";
import { useSignInMutationMutation } from "../graphql/user.graphql";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { MdLockOutline } from "react-icons/md";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const Login: FC = () => {
    const client = useApolloClient();
    const [signIn] = useSignInMutationMutation();
    const [errorMsg, setErrorMsg] = useState();
    const router = useRouter();

    const classes = useStyles();

    async function handleSubmit(event) {
        event.preventDefault();

        const usernameElement = event.currentTarget.elements.username;
        const passwordElement = event.currentTarget.elements.password;

        try {
            await client.resetStore();
            const { data } = await signIn({
                variables: {
                    username: usernameElement.value,
                    password: passwordElement.value,
                },
            });
            if (data.signIn.user) {
                await router.push("/");
            }
        } catch (error) {
            setErrorMsg(getErrorMessage(error));
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <MdLockOutline />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <form onSubmit={handleSubmit}>
                    {errorMsg && <p>{errorMsg}</p>}
                    <TextField
                        name="username"
                        type="text"
                        autoComplete="username"
                        required
                        label="Username"
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        id="email"
                        autoFocus
                    />
                    <TextField
                        name="password"
                        type="password"
                        autoComplete="password"
                        required
                        label="Password"
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        id="password"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign in
                    </Button>
                    <Link href="signup">
                        <a>Don't have an account? Sign Up</a>
                    </Link>
                </form>
            </div>
        </Container>
    );
};
export default Login;
