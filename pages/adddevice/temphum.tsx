import { FC, useState } from "react";
import router from "next/router";
import { useAddTempHumMutation } from "../../graphql/tempHum.graphql";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { getErrorMessage } from "../../lib/form";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const tempHum: FC = () => {
    const classes = useStyles();

    const [addTempHum] = useAddTempHumMutation();
    const [errorMsg, setErrorMsg] = useState();

    async function handleSubmit(event) {
        event.preventDefault();

        const nameElement = event.currentTarget.elements.name;
        const ipElement = event.currentTarget.elements.ip;

        try {
            await addTempHum({
                variables: {
                    name: nameElement.value,
                    ip: ipElement.value,
                },
            });
            router.push("/");
        } catch (error) {
            setErrorMsg(getErrorMessage(error));
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Add ESP8266 with DHT22
                </Typography>
                <form onSubmit={handleSubmit}>
                    {errorMsg && <p>{errorMsg}</p>}
                    <TextField
                        name="name"
                        type="text"
                        required
                        label="Name"
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        id="name"
                        autoFocus
                    />

                    <TextField
                        name="ip"
                        type="text"
                        required
                        label="Ip"
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        id="ip"
                    />

                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Add
                    </Button>
                </form>
            </div>
        </Container>
    );
};
export default tempHum;
