import { FC } from "react";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { useApolloClient } from "@apollo/client";
import { useSignOutMutationMutation } from "../graphql/user.graphql";
import { Loading } from "../components/widgets/loading";

const SignOut: FC = () => {
    const client = useApolloClient();
    const router = useRouter();
    const [signOut] = useSignOutMutationMutation();

    useEffect(() => {
        signOut().then(() => {
            client.resetStore().then(() => {
                router.push("/login");
            });
        });
    }, [signOut, router, client]);

    return <Loading />;
};

export default SignOut;
