import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, FC } from "react";
import Dashboard from "../components/dashboard";
import { useViewerQueryQuery } from "../graphql/user.graphql";
import { MdPerson, MdAdd } from "react-icons/md";

import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import { Loading } from "../components/widgets/loading";

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    title: {
        flexGrow: 1,
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: "100vh",
        overflow: "auto",
    },
}));

const Home: FC = () => {
    const classes = useStyles();

    const router = useRouter();
    const { data, loading, error } = useViewerQueryQuery();
    const viewer = data?.viewer;
    const shouldRedirect = !(loading || error || viewer);

    useEffect(() => {
        if (shouldRedirect) {
            router.push("/login");
        }
    }, [shouldRedirect]);

    if (error) {
        return <p>{error.message}</p>;
    }

    if (viewer)
        return (
            <div className={classes.root}>
                <CssBaseline />
                <Head>
                    <title>Home Centre</title>
                    <link rel="icon" href="/favicon.ico" />
                </Head>
                <AppBar>
                    <Toolbar className={classes.toolbar}>
                        <Typography
                            component="h1"
                            variant="h6"
                            color="inherit"
                            noWrap
                            className={classes.title}
                        >
                            HomeCentre
                        </Typography>
                        <IconButton color="inherit" href="/adddevice">
                            <MdAdd />
                        </IconButton>
                        <IconButton color="inherit" href="/logout">
                            <MdPerson />
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <main className={classes.content}>
                    <div className={classes.appBarSpacer} />
                    <Dashboard />
                </main>
            </div>
        );
    return <Loading />;
};
export default Home;
